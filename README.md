# Congestion graphs with AVs

This is supplementary material for a paper under review.
Title: 'Departure Time Choice and Bottleneck Congestion with Automated Vehicles: Role of On-board Activities'
The material contains MATLAB codes to re-create the figures of this paper. Code was created in MATLAB R2018b.