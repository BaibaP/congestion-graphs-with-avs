% This code can be executed all at once to create Figures 6-9 from the paper.
% The running time of the code is about 3 minutes.
% When creating separate figures, start by once executing the 'General section' (just
% below), and then proceed to the section for desired figure.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GENERAL SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
syms t;
t_star = 50; % preferred arrival time
omega = 100; % end of morning interval
gran = 0.5; % granualirity of computation and graphs
N=200; %# of travellers
s=5; % capacity of the bottleneck

% Scheduling parameters:
alpha = 2;
beta = 1;
gamma = 4;
eh = 0.3;
ew = 0.3;
low = t_star-gamma*N/((beta+gamma)*s); % congestion start
up = t_star+beta*N/((beta+gamma)*s); % congestion end

% Conventional vehicle
wave_0 = t_star-beta*gamma*N/(alpha*(beta+gamma)*s);% undelayed departure time
r_0 = piecewise(low<=t<=wave_0, beta/(alpha-beta), wave_0<t<=up, -gamma/(alpha+gamma), 0); % flow rate
int_r_0 = @(u) int(r_0,t,0,u); % number of departed travellers until time u - a function
int_r_array_0 = arrayfun(int_r_0,[0:gran:omega]); % number of departed travellers, at time points of an interval

% Home AV
wave_1 = t_star-beta*gamma*N/(alpha*(1-eh)*(beta+gamma)*s);
r_1 = piecewise(low<=t<=wave_1, beta/(alpha*(1-eh)-beta), wave_1<t<=up, -gamma/(alpha*(1-eh)+gamma), 0);
int_r_1 = @(u) int(r_1,t,0,u);
int_r_array_1 = arrayfun(int_r_1,[0:gran:omega]);

% Universal AV
wave_2 = wave_1;
r_2 = piecewise(low<=t<=wave_2, beta/(alpha*(1-eh)-beta), wave_2<t<=t_star, alpha*(1-eh)/((alpha+gamma)*(1-ew))-1, t_star<t<=up, -gamma/((alpha+gamma)*(1-ew)), 0);
int_r_2 = @(u) int(r_2,t,0,u);
int_r_array_2 = arrayfun(int_r_2,[0:gran:omega]);

% Work AV
wave_3 = t_star-beta*gamma*N/((alpha*(1-ew)+beta*ew)*(beta+gamma)*s);
r_3 = piecewise(low<=t<=wave_3, beta/((alpha-beta)*(1-ew)), wave_3<t<=t_star, (alpha*(1-ew)+beta*ew)/((alpha+gamma)*(1-ew))-1, t_star<t<=up, (alpha*(1-ew)-gamma*ew)/((alpha+gamma)*(1-ew))-1, 0);
int_r_3 = @(u) int(r_3,t,0,u);
int_r_array_3 = arrayfun(int_r_3,[0:gran:omega]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF GENERAL SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE 6 - plot of ALL congestion graphs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(6);
plot([0:gran:100], int_r_array_0,'LineWidth',1,'Color','black')
hold on

% Frame 
xlim([15 60]);
xlabel('Departure time');
ylim([-3 30]);
ylabel('Queueing time');

% Supporting (faint) lines
plot([27.25 50], [22.75 0],'-','LineWidth',0.3,'Color', [0.8 0.8 0.8],'HandleVisibility','off');
plot([0 100], [0 0],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
plot([50 50], [-1 1],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
text(50.2,-1,'t*');

% Main graphs
plot([0:gran:100], int_r_array_1,'--','LineWidth',1.5,'Color','black');
plot([0:gran:100], int_r_array_2,'o','MarkerSize',3,'LineWidth',1,'Color','black');
plot([0:gran:100], int_r_array_3,':','LineWidth',1.5,'Color','black');

hold off

legend({'Conventional vehicle','Home AV: eh = 0.3, ew = 0','Universal AV: eh = ew = 0.3','Work AV: eh = 0, ew = 0.3'},'Location','northwest');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF FIGURE 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE 7 - mixture of CONVENTIONAL vehicles and HOME AVs %%%%%%%%%%%%%%%%%%%%%
syms t tA tC;
syms lowx upx wavex;
e = 0.3; % For mixture graphs a single efficiency parameter is used. eh = ew = e
% The original graphs are taken from General section (using eh, ew defined therein).

% Finding the 2 points on x-axis, where both graphs cross: tA and tC
% # of conventional vehicle users departing before tA and after tC. Flow rate * intervals
eq1 = (alpha/(alpha-beta))*s*(tA-low)+(alpha/(alpha+gamma))*s*(up-tC);
% Queue lengths at tA and tC. Queue formula using conventional preferences
queA = beta*(tA-low)/(alpha-beta);
queC = beta*(wave_0-low)/(alpha-beta)+((alpha/(alpha+gamma))-1)*(tC-wave_0);
% Home AV costs (sum of travel time and schedule delay costs) at tA and tC should be equal. eq2 defines their difference
eq2 = queA*(alpha*(1-e)-beta)+(t_star-tA)*beta-queC*(alpha*(1-e)+gamma)-(tC-t_star)*gamma;
[tA,tC] = solve(eq1 == 100, eq2 == 0);
% Convert results to numeric values
tA = double(tA);
tC = double(tC);
queA = beta*(tA-low)/((alpha-beta));
queC = beta*(wave_0-low)/(alpha-beta)+(alpha/(alpha+gamma)-1)*(tC-wave_0);

% Calculating parameters for the modified Home preferences: lowx (start of graph) ,upx (end of graph), wavex (undelayed time).
% Queue at tA according to Home preferences
eq3 = (beta/(alpha*(1-e)-beta))*(tA-lowx);
% Queue at tC according to Home preferences (building up from the right)
eq4 = (gamma/(alpha*(1-e)+gamma))*(upx - tC);
% Queue at undelayed time: equal when calculated from left and right
eq5 = (wavex-lowx)*(beta/(alpha*(1-e)-beta))-(upx-wavex)*(gamma/(alpha*(1-e)+gamma));
[lowx,upx,wavex] = solve(eq3 == queA, eq4 == queC, eq5 == 0);
lowx = double(lowx);
upx = double(upx);
wavex = double(wavex);

% Modified Home vector
r_x = piecewise(lowx<=t<=wavex, beta/(alpha*(1-e)-beta), wavex<t<=upx, -gamma/(alpha*(1-e)+gamma), 0);
int_r_x = @(u) int(r_x,t,0,u);
int_r_array_x = arrayfun(int_r_x,[0:gran:omega]);

%%%%%% PLOT %%%%%%
figure(7);
plot([0:gran:100], int_r_array_1,'--','LineWidth',1.5,'Color',[0.6 0.6 0.6]);
hold on

% Frame
xlim([15 60]);
xlabel('Departure time');
ylim([-3 30]);
ylabel('Queueing time');

% Supporting (faint) lines
plot([0 100], [0 0],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
plot([50 50], [-1 1],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
text(50.2,-1,'t*');
plot([27.5 50], [22.5 0],'-','LineWidth',0.3,'Color', [0.8 0.8 0.8],'HandleVisibility','off');

% Main graphs
plot([0:gran:100], int_r_array_x,'--','LineWidth',1.5,'Color','black');
plot([0:gran:100], int_r_array_0,'LineWidth',1,'Color','black');

% Travel Equilibrium Frontier
plot([0:gran:(round(double(tA)))], int_r_array_0(1:((round(double(tA))+gran)/gran)),'LineWidth',2.5,'Color','black');
plot([(round(double(tA))):gran:(round(double(tC)))], int_r_array_x(((round(double(tA))+gran)/gran):((round(double(tC))+gran)/gran)),'LineWidth',2.5,'Color','black');
plot([(round(double(tC))-gran):gran:(100-gran)], int_r_array_0((round(double(tC))/gran):(100/gran)),'LineWidth',2.5,'Color','black');

hold off

legend({'Home AV: eh = 0.3, ew = 0, N = 200 (original)','Home AV: eh = 0.3, ew = 0, N = 100 (modified)','Conventional vehicle, N = 100','Travel Equilibrium Frontier'},'Location','northwest');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF FIGURE 7 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE 8 - mixture of CONVENTIONAL vehicles and WORK AVs %%%%%%%%%%%%%%%%%%%%%
syms t tA tC;
syms lowx upx wavex;
e = 0.3;

% Finding the 2 points on x-axis, where both graphs cross: tA and tC
% # of conventional vehicle users departing before tA and after tC. Flow rate * intervals
eq1 = (alpha/(alpha-beta))*s*(tA-low)+(alpha/(alpha+gamma))*s*(up-tC);
% Queue lengths at tA and tC. Queue formula using conventional preferences
queA = beta*(tA-low)/(alpha-beta);
queC = beta*(wave_0-low)/(alpha-beta)+((alpha/(alpha+gamma))-1)*(tC-wave_0);
% Work AV costs (sum of travel time and schedule delay costs) at tA and tC should be equal. eq2 defines their difference
costsA = queA*((alpha-beta)*(1-e))+(t_star-tA)*beta;
costsC = queC*((alpha+gamma)*(1-e))+(tC-t_star)*gamma;
eq2 = costsA-costsC;
[tA,tC] = solve(eq1 == 100, eq2 == 0);
% Convert results to numeric values
tA = double(tA);
tC = double(tC);
queA = beta*(tA-low)/((alpha-beta));
queC = beta*(wave_0-low)/(alpha-beta)+(alpha/(alpha+gamma)-1)*(tC-wave_0);

% Calculating parameters for the modified Work preferences: lowx (start of graph) ,upx (end of graph), wavex (undelayed time).
% Queue at tA according to Work preferences
eq3 = (beta/((alpha-beta)*(1-e)))*(tA-lowx);
% Queue at tC according to Work preferences (building up from the right)
eq4 = -(alpha/(alpha+gamma)-gamma*e/((alpha+gamma)*(1-e))-1)*(upx - tC);
% Queue at undelayed time: equal when calculated from left and right
eq5 = (wavex-lowx)*(beta/((alpha-beta)*(1-e)))+(upx-t_star)*(alpha/(alpha+gamma)-gamma*e/((alpha+gamma)*(1-e))-1)+(t_star-wavex)*(alpha/(alpha+gamma)+beta*e/((alpha+gamma)*(1-e))-1);
[lowx,upx,wavex] = solve(eq3 == queA, eq4 == queC, eq5 ==0);
lowx = double(lowx);
upx = double(upx);
wavex = double(wavex);

% Modified Work vector
r_x = piecewise(lowx<=t<=wavex, beta/((alpha-beta)*(1-e)), wavex<t<=t_star, alpha/(alpha+gamma)+beta*e/((alpha+gamma)*(1-e))-1, t_star<t<=upx, alpha/(alpha+gamma)-gamma*e/((alpha+gamma)*(1-e))-1, 0);
int_r_x = @(u) int(r_x,t,0,u);
int_r_array_x = arrayfun(int_r_x,[0:gran:omega]);


%%%%%% PLOT %%%%%%
figure(8);
plot([0:gran:100], int_r_array_3,':','LineWidth',1.5,'Color',[0.6 0.6 0.6]);
hold on

% Frame
xlim([15 60]);
xlabel('Departure time');
ylim([-3 30]);
ylabel('Queueing time');

% Supporting (faint) lines
plot([0 100], [0 0],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
plot([50 50], [-1 1],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
text(50.2,-1,'t*');
plot([31.5 50], [18.5 0],'-','LineWidth',0.3,'Color', [0.8 0.8 0.8],'HandleVisibility','off');

% Main graphs
plot([0:gran:100], int_r_array_x,':','LineWidth',1.5,'Color','black')
plot([0:gran:100], int_r_array_0,'LineWidth',1,'Color','black');

% Travel Equilibrium Frontier
plot([0:gran:(round(double(tA))-gran)], int_r_array_0(1:(round(double(tA))/gran)),'LineWidth',2.5,'Color','black');
plot([(round(double(tA))-gran):gran:(round(double(tC))-gran)], int_r_array_x((round(double(tA))/gran):(round(double(tC))/gran)),'LineWidth',2.5,'Color','black');
plot([(round(double(tC))-gran):gran:(100-gran)], int_r_array_0((round(double(tC))/gran):(100/gran)),'LineWidth',2.5,'Color','black');

hold off

legend({'Work AV: eh = 0, ew = 0.3 (original)','Work AV: eh = 0, ew = 0.3 (modified)','Conventional vehicle','Travel Equilibrium Frontier'},'Location','northwest');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF FIGURE 8 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIGURE 9 - mixture of HOME vehicles and WORK AVs %%%%%%%%%%%%%%%%%%%%%%%%%%
syms t tA tC;
syms lowx upx wavex;
e = 0.3;

% Finding the 2 points on x-axis, where both graphs cross: tA and tC
% # of Work AV users departing before tA and after tC. Flow rate * intervals.
% 2 segments of flow rate need to be considered after tC
eq1 = (1+ beta/((alpha-beta)*(1-e)))*s*(tA-low)+(alpha/(alpha+gamma)+beta*e/((alpha+gamma)*(1-e)))*s*(t_star-tC)+(alpha/(alpha+gamma)-gamma*e/((alpha+gamma)*(1-e)))*s*(up-t_star);
% Queue lengths at tA and tC. Queue formula using Work preferences
queA = beta*(tA-low)/((alpha-beta)*(1-e));
queC = beta*(wave_3-low)/((alpha-beta)*(1-e))+(alpha/(alpha+gamma)+beta*e/((alpha+gamma)*(1-e))-1)*(tC-wave_3);
% Home AV costs (sum of travel time and schedule delay costs) at tA and tC should be equal. eq2 defines their difference
eq2 = queA*(alpha*(1-e)-beta)+(t_star-tA)*beta-queC*(alpha*(1-e)+gamma)-(tC-t_star)*gamma;
[tA,tC] = solve(eq1 == 100, eq2 == 0);
% Convert results to numeric values
tA = double(tA);
tC = double(tC);
queA = beta*(tA-low)/((alpha-beta)*(1-e));
queC = beta*(wave_3-low)/((alpha-beta)*(1-e))+(alpha/(alpha+gamma)+beta*e/((alpha+gamma)*(1-e))-1)*(tC-wave_3);

% Calculating parameters for the modified Home preferences: lowx (start of graph) ,upx (end of graph), wavex (undelayed time).
% Queue at tA according to Home preferences
eq3 = (beta/(alpha*(1-e)-beta))*(tA-lowx);
% Queue at tC according to Home preferences (building up from the right)
eq4 = (gamma/(alpha*(1-e)+gamma))*(upx - tC);
% Queue at undelayed time: equal when calculated from left and right
eq5 = (wavex-lowx)*(beta/(alpha*(1-e)-beta))-(upx-wavex)*(gamma/(alpha*(1-e)+gamma));
[lowx,upx,wavex] = solve(eq3 == queA, eq4 == queC, eq5 ==0);
lowx = double(lowx);
upx = double(upx);
wavex = double(wavex);

% Modified Home vector
r_x = piecewise(lowx<=t<=wavex, beta/(alpha*(1-e)-beta), wavex<t<=upx, -gamma/(alpha*(1-e)+gamma), 0);
int_r_x = @(u) int(r_x,t,0,u);
int_r_array_x = arrayfun(int_r_x,[0:gran:omega]);


%%%%%% PLOT %%%%%%
figure(9);
plot([0:gran:100], int_r_array_1,'--','LineWidth',1.5,'Color',[0.6 0.6 0.6]);
hold on

% Frame
xlim([15 60]);
xlabel('Departure time');
ylim([-3 30]);
ylabel('Queueing time');

% Supporting (faint) lines
plot([0 100], [0 0],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
plot([50 50], [-1 1],'-','LineWidth',1,'Color', [0.6 0.6 0.6],'HandleVisibility','off');
text(50.2,-1,'t*');
plot([27.25 50], [22.75 0],'-','LineWidth',0.3,'Color', [0.8 0.8 0.8],'HandleVisibility','off'); %line connecting the t-star

% Main graphs
plot([0:gran:100], int_r_array_x,'--','LineWidth',1.5,'Color','black')
plot([0:gran:100], int_r_array_3,':','LineWidth',1.5,'Color','black');

% Travel Equilibrium Frontier
plot([0:gran:(round(double(tA))-gran)], int_r_array_3(1:(round(double(tA))/gran)),'LineWidth',2.5,'Color','black');
plot([(round(double(tA))-gran):gran:(round(double(tC)))], int_r_array_x((round(double(tA))/gran):(round(double(tC))/gran+1)),'LineWidth',2.5,'Color','black');
plot([round(double(tC)):gran:100], int_r_array_3((round(double(tC))/gran+1):(100/gran+1)),'LineWidth',2.5,'Color','black');

hold off

legend({'Home AV: eh = 0.3, ew = 0 (original)','Home AV: eh = 0.3, ew = 0 (modified)','Work AV: eh = 0, ew = 0.3','Travel Equilibrium Frontier'},'Location','northwest');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF FIGURE 9 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%